all: docker-image

docker-image: docker/Dockerfile docker/install
	docker build -t mawis/coleslaw docker

push:
	docker push mawis/coleslaw:latest
