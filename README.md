Coleslaw container to build my blog
===================================

This is a container I use within [Drone](https://drone.io/) to build my website
when there are new commits to its repository.
